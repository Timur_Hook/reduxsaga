import React from 'react';
import {App2} from './src/pages';
import {Provider} from 'react-redux';
import configureStore from './src/store/Configuration';
import createSagaMiddleware from 'redux-saga'
import {watcherSaga} from './src/sagas/sagas';


const App = () => {
  const sagaMiddleware = createSagaMiddleware()
  const store = configureStore(sagaMiddleware);
  sagaMiddleware.run(watcherSaga)
  return (
    <Provider store={store}>
      <App2 />
    </Provider>
  );
};



export default App;
