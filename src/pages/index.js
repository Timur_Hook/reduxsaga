import React from 'react';
import { TouchableOpacity } from 'react-native';
import {View, Text} from 'react-native';
import { useDispatch } from 'react-redux'
import { SAGA_START } from './../constants';

const App2 = () => {
    const dispatch = useDispatch();
    return <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity onPress={() => dispatch({type: 'SAGA_START'})} 
            style={{borderWidth: 1, borderColor: 'red', width: '80%', height: 40, justifyContent: 'center'}}>
            <Text style={{textAlign: 'center'}}>Click</Text>
        </TouchableOpacity>
        <Text style={{borderWidth: 1, borderColor: 'red'}}>Here</Text>
    </View>
}

export {App2}