import {takeEvery, put, call} from 'redux-saga/effects';
import {apiGet} from './../config/Api';
import {SAGA_START, baseUrl, ADD_TODO} from './../constants';

export function* watcherSaga() {
    yield takeEvery(SAGA_START, workerSaga)
}

function* workerSaga() {
  const data = yield call(fetchData);
  yield put({
    type: ADD_TODO,
    payload: data
  })
}

function fetchData() {
  // return fetch('https://jsonplaceholder.typicode.com/todos/1')
  //         .then(response => response.json());
  console.log('%c++[        ]','background: violet', 'here');
  return apiGet('todos/1');
}