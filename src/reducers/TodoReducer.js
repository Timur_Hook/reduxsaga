import {ADD_TODO} from './../constants';
export const TodoReducer = (state = {}, action) => {
    console.log('%c++[   ADD_TODO     ]','background: red', action);
    switch (action.type) {
      case ADD_TODO:
        return {...action.payload};
      default:
        return state;
    }
  }