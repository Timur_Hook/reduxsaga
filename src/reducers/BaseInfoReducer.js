const BASE_INFO = 'BASE_INFO';
const INITIAL_STATE = {
    messages: 0,
    notifications: 0,
    orders:{
        done: 0,
        inWork: 0,
        new: 0,
        onCheck: 0
    }
};

export const baseInfoReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case BASE_INFO:
        return {...action.payload};
      default:
        return state;
    }
  }