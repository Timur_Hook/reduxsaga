import {combineReducers} from 'redux';
import {baseInfoReducer} from './BaseInfoReducer';
import {TodoReducer} from './TodoReducer';

export default combineReducers({
    baseInfo: baseInfoReducer,
    todo: TodoReducer
});