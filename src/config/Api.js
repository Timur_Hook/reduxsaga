import axios from 'axios';
import {baseUrl} from './../constants';
export let USER_OBJECT = {access_token: ''};

function getHeaders() {
    console.log('%c++USER_OBJECT', 'background:red', USER_OBJECT);
    let header = {
        'Content-Type': 'multipart/form-data',
        'Request-From': 'APP'
    };
    header.Authorization = `Bearer ${USER_OBJECT.access_token}`;
    return header;
}

export const apiGet = async (fetchUrl, dataObject = {}, showError) => {
    const headers = await getHeaders();
    console.log('Url: ', `${baseUrl}${fetchUrl}`, 'Тело запроса: ', dataObject, 'headers: ', headers);
    let result = await axios.get(
            `${baseUrl}${fetchUrl}`,
            {
                params: dataObject,
                headers,
                timeout: 30000
            }
        ).then(response => {
            console.log(response);
            return response.data;
        }).catch((error) => {
            console.log(error.response);
            handleErrors(error.response.data);
            return false;
        });
    if (result) {
        return result;
    } else {
        handleErrors(result, () => apiPost(fetchUrl, dataObject));
    }
}

export const apiPost = async (fetchUrl, dataObject = {}, showError) => {
    const headers = getHeaders();
    console.log('Url: ', `${baseUrl}${fetchUrl}`, 'Тело запроса: ', dataObject, 'headers: ', headers);
    let data = await axios.post(
            `${baseUrl}${fetchUrl}`,
            dataObject,
            {
                headers,
                timeout: 20000
            }
        ).then((result)=>{
            console.log('%c++ apiPost ============', 'background:lime', result);
          return result.data;
        }).catch((err) => {
            console.log('%c++erraarfeqer%$54', 'background:red', err.response);
          if(showError !== false){
            handleErrors(err.response.data, () => apiPost(fetchUrl, dataObject))
          }
        })
        
        return data || false;
}